package com.maddev.news_reader.domain.interactor

import com.maddev.news_reader.data.entity.NewsCategory.BUSINESS
import com.maddev.news_reader.data.entity.NewsCategory.SPORT
import com.maddev.news_reader.data.repository.NewsRepository
import com.maddev.news_reader.domain.entity.Article
import com.maddev.news_reader.utils.SchedulersProvider
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Single
import io.reactivex.internal.schedulers.TrampolineScheduler
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.sql.Date

@RunWith(MockitoJUnitRunner::class)
class NewsInteractorTest {

    @Mock
    private lateinit var repo: NewsRepository

    @Mock
    private lateinit var schedulersProvider: SchedulersProvider

    private val testScheduler = TrampolineScheduler.instance()

    private lateinit var sut: NewsInteractorImpl

    @Before
    fun setup() {
        sut = NewsInteractorImpl(repo, schedulersProvider)
        whenever(schedulersProvider.io).thenReturn(testScheduler)
    }

    @Test
    fun `Should return list with sport and business news`() {
        // given
        val article1 = Article("a", "b", "c", "d", "e", Date(1))
        val article2 = Article("a", "b", "c", "d", "e", Date(2))
        whenever(repo.getNews(SPORT)).thenReturn(Single.just(listOf(article1)))
        whenever(repo.getNews(BUSINESS)).thenReturn(Single.just(listOf(article2)))

        // when
        val subscriber = sut.getNews().test()

        // then
        subscriber.assertValue { list ->
            list.contains(article1)
            list.contains(article2)
        }
    }
}