package com.maddev.news_reader.domain.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
data class Article(
    val title: String?,
    val source: String?,
    val author: String?,
    val desc: String?,
    val image: String?,
    val date: Date
) : Parcelable