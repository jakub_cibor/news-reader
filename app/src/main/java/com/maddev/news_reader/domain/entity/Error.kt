package com.maddev.news_reader.domain.entity

sealed class Error : Throwable()

object UnknownError : Error()

object NoConnectionError : Error()

class HttpError(val description: String?) : Error()
