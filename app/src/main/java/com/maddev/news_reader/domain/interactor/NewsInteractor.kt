package com.maddev.news_reader.domain.interactor

import com.maddev.news_reader.data.entity.NewsCategory.BUSINESS
import com.maddev.news_reader.data.entity.NewsCategory.SPORT
import com.maddev.news_reader.data.repository.NewsRepository
import com.maddev.news_reader.domain.entity.Article
import com.maddev.news_reader.utils.SchedulersProvider
import io.reactivex.Single
import io.reactivex.rxkotlin.Singles

interface NewsInteractor {
    fun getNews(): Single<List<Article>>
}

class NewsInteractorImpl(private val repo: NewsRepository, private val schedulersProvider: SchedulersProvider) : NewsInteractor {

    override fun getNews(): Single<List<Article>> =
        Singles.zip(
            repo.getNews(BUSINESS),
            repo.getNews(SPORT)
        ) { business, sport ->
            business.union(sport).toList()
        }
            .subscribeOn(schedulersProvider.io)
}