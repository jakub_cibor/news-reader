package com.maddev.news_reader.data.api

import io.reactivex.Completable
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.Function
import retrofit2.Call
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import java.lang.reflect.Type

internal class RxCallAdapterFactory private constructor(private val errorMapper: HttpErrorMapper) :
    CallAdapter.Factory() {

    companion object {
        fun create(errorMapper: HttpErrorMapper): CallAdapter.Factory = RxCallAdapterFactory(errorMapper)
    }

    private val original = RxJava2CallAdapterFactory.create()

    override fun get(returnType: Type, annotations: Array<Annotation>, retrofit: Retrofit): CallAdapter<*, *>? {
        return RxCallAdapterWrapper(original.get(returnType, annotations, retrofit) ?: return null)
    }

    private inner class RxCallAdapterWrapper<R>(private val wrapped: CallAdapter<R, *>) : CallAdapter<R, Any> {

        override fun responseType(): Type = wrapped.responseType()

        override fun adapt(call: Call<R>): Any = when (val result = wrapped.adapt(call)) {
            is Single<*> ->
                result.onErrorResumeNext(Function { throwable -> Single.error(mapError(throwable)) })
            is Observable<*> ->
                result.onErrorResumeNext(Function { throwable -> Observable.error(mapError(throwable)) })
            is Flowable<*> ->
                result.onErrorResumeNext(Function { throwable -> Flowable.error(mapError(throwable)) })
            is Completable ->
                result.onErrorResumeNext(Function { throwable -> Completable.error(mapError(throwable)) })
            else ->
                result
        }

        private fun mapError(throwable: Throwable) = errorMapper.mapError(throwable)
    }
}