package com.maddev.news_reader.data.entity.dto

import com.google.gson.annotations.SerializedName
import java.util.*

data class ArticleDto(
    @SerializedName("author")
    val author: String?,
    @SerializedName("content")
    val content: String?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("publishedAt")
    val publishedAt: Date,
    @SerializedName("source")
    val source: Source?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("url")
    val url: String?,
    @SerializedName("urlToImage")
    val urlToImage: String?
) {
    data class Source(
        @SerializedName("id")
        val id: Any?,
        @SerializedName("name")
        val name: String?
    )
}