package com.maddev.news_reader.data.repository

import com.maddev.news_reader.data.api.Api
import com.maddev.news_reader.data.entity.NewsCategory
import com.maddev.news_reader.data.entity.dto.ArticleDto
import com.maddev.news_reader.domain.entity.Article
import io.reactivex.Single
import java.util.*

interface NewsRepository {
    fun getNews(category: NewsCategory): Single<List<Article>>
}

class NewsRepositoryImpl(private val api: Api) : NewsRepository {

    override fun getNews(category: NewsCategory): Single<List<Article>> =
        api.getNews(DEFAULT_COUNTRY_CODE, category.name.toLowerCase(Locale.ROOT))
            .map { news -> news.articles?.map { it.toArticle() } ?: emptyList() }
}

private fun ArticleDto.toArticle() = Article(
    title = title,
    source = source?.name,
    author = author,
    desc = description,
    image = urlToImage,
    date = publishedAt
)

private const val DEFAULT_COUNTRY_CODE = "us"