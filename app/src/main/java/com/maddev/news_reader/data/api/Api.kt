package com.maddev.news_reader.data.api

import com.maddev.news_reader.data.Const.Api.API_KEY
import com.maddev.news_reader.data.Const.Api.Endpoint
import com.maddev.news_reader.data.Const.Api.Param
import com.maddev.news_reader.data.entity.dto.NewsDto
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface Api {

    @GET(Endpoint.TOP_HEADLINES)
    fun getNews(
        @Query(Param.COUNTRY) countryCode: String,
        @Query(Param.CATEGORY) category: String,
        @Query(Param.API_KEY) apiKey: String = API_KEY
    ): Single<NewsDto>
}