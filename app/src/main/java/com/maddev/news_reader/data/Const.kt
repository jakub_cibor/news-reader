package com.maddev.news_reader.data

object Const {

    object Api {
        const val URL = "https://newsapi.org/v2/"
        const val API_KEY = "b073eb2291914a62a6616e309bad2397"

        object Endpoint {
            const val TOP_HEADLINES = "top-headlines"
        }

        object Param {
            const val COUNTRY = "country"
            const val CATEGORY = "category"
            const val API_KEY = "apiKey"
        }
    }
}