package com.maddev.news_reader.data.entity

enum class NewsCategory {
    BUSINESS, SPORT
}