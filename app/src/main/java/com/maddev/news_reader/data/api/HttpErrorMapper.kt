package com.maddev.news_reader.data.api

import com.maddev.news_reader.domain.entity.HttpError
import com.maddev.news_reader.domain.entity.NoConnectionError
import retrofit2.HttpException
import java.io.IOException

class HttpErrorMapper {

    fun mapError(throwable: Throwable) = when (throwable) {
        is HttpException -> HttpError(throwable.response().errorBody()?.string())
        is IOException -> NoConnectionError
        else -> throwable
    }
}