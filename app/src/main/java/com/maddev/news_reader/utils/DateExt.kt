package com.maddev.news_reader.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SimpleDateFormat")
fun Date.format(): String =
    SimpleDateFormat(DATA_FORMAT).format(this)

private const val DATA_FORMAT = "yyyy-MM-dd HH:mm"