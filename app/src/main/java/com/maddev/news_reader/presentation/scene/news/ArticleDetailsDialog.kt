package com.maddev.news_reader.presentation.scene.news

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import com.maddev.news_reader.R
import com.maddev.news_reader.domain.entity.Article
import com.maddev.news_reader.presentation.utils.addBundle
import com.maddev.news_reader.presentation.utils.load
import com.maddev.news_reader.presentation.utils.set
import com.maddev.news_reader.utils.format
import kotlinx.android.synthetic.main.dialog_article.*

class ArticleDetailsDialog : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.AppTheme_Dialog)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.dialog_article, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val article = arguments?.getParcelable<Article>(ARG_ARTICLE) ?: return
        image.load(article.image)
        date.text = article.date.format()
        author.set(article.author)
        source.set(article.source)
        title.set(article.title)
        desc.set(article.desc)
    }

    companion object {
        fun init(article: Article) = ArticleDetailsDialog().addBundle {
            putParcelable(ARG_ARTICLE, article)
        }
    }
}

private const val ARG_ARTICLE = "article"