package com.maddev.news_reader.presentation.scene.news

import android.os.Bundle
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.maddev.news_reader.domain.entity.Article
import com.maddev.news_reader.domain.entity.Error
import com.maddev.news_reader.domain.entity.UnknownError
import com.maddev.news_reader.domain.interactor.NewsInteractor
import com.maddev.news_reader.presentation.base.BaseViewModel
import com.maddev.news_reader.utils.SchedulersProvider

class NewsViewModel(private val newsInteractor: NewsInteractor, private val schedulersProvider: SchedulersProvider) : BaseViewModel() {

    private val _loading: MutableLiveData<Boolean> = MutableLiveData()
    val loading: LiveData<Boolean> = _loading

    private val _error: MutableLiveData<Error> = MutableLiveData()
    val error: LiveData<Error> = _error

    private val _news: MutableLiveData<List<Article>> = MutableLiveData()
    val news: LiveData<List<Article>> = _news

    override fun onBind(arguments: Bundle?) {
        loadNews()
    }

    fun loadNews() {
        newsInteractor.getNews()
            .observeOn(schedulersProvider.ui)
            .doOnSubscribe { notifyProgress(true) }
            .doFinally { notifyProgress(false) }
            .map { list -> list.sortedByDescending { it.date } }
            .subscribe(
                ::onSuccess,
                ::onError
            )
            .register()
    }

    private fun notifyProgress(loading: Boolean) {
        _loading.value = loading
    }

    private fun onSuccess(data: List<Article>) {
        _news.value = data
    }

    private fun onError(error: Throwable) {
        _error.value = error as? Error ?: UnknownError
    }
}