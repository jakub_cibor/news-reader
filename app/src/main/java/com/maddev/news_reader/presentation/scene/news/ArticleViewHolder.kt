package com.maddev.news_reader.presentation.scene.news

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.maddev.news_reader.domain.entity.Article
import com.maddev.news_reader.presentation.utils.load
import com.maddev.news_reader.presentation.utils.set
import com.maddev.news_reader.utils.format
import kotlinx.android.synthetic.main.viewholder_article.view.*

class ArticleViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    fun bind(article: Article) {
        with(itemView) {
            image.load(article.image)
            title.set(article.title)
            desc.set(article.desc)
            date.text = article.date.format()
        }
    }
}