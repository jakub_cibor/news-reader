package com.maddev.news_reader.presentation.scene.news

import android.os.Bundle
import android.view.View
import android.widget.Toast
import android.widget.Toast.LENGTH_LONG
import androidx.annotation.StringRes
import androidx.recyclerview.widget.LinearLayoutManager
import com.maddev.news_reader.R
import com.maddev.news_reader.domain.entity.Article
import com.maddev.news_reader.domain.entity.Error
import com.maddev.news_reader.domain.entity.HttpError
import com.maddev.news_reader.domain.entity.NoConnectionError
import com.maddev.news_reader.presentation.base.BaseFragment
import com.maddev.news_reader.utils.observe
import kotlinx.android.synthetic.main.fragment_news.*

class NewsFragment : BaseFragment<NewsViewModel>(NewsViewModel::class, R.layout.fragment_news), NewsAdapter.OnClickListener {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val newsAdapter = NewsAdapter(this)

        recyclerView.apply {
            adapter = newsAdapter
            layoutManager = LinearLayoutManager(requireContext())
        }

        pullToRefresh.setOnRefreshListener {
            viewModel.loadNews()
        }

        viewModel.loading.observe(viewLifecycleOwner) { loading ->
            pullToRefresh.isRefreshing = loading
        }

        viewModel.error.observe(viewLifecycleOwner) { error ->
            handleError(error)
        }

        viewModel.news.observe(viewLifecycleOwner) { news ->
            recyclerView.visibility = View.VISIBLE
            newsAdapter.submitList(news)
        }
    }

    override fun onClick(article: Article) {
        if (parentFragmentManager.findFragmentByTag(ArticleDetailsDialog::class.simpleName) == null) {
            ArticleDetailsDialog.init(article).show(parentFragmentManager, ArticleDetailsDialog::class.simpleName)
        }
    }

    private fun handleError(error: Error) {
        when (error) {
            is HttpError -> showToast(getString(R.string.server_error, error.description))
            is NoConnectionError -> showToast(R.string.no_connection_error)
            else -> showToast(R.string.unknown_error)
        }
    }

    private fun showToast(@StringRes message: Int) {
        Toast.makeText(requireContext(), message, LENGTH_LONG).show()
    }

    private fun showToast(message: String) {
        Toast.makeText(requireContext(), message, LENGTH_LONG).show()
    }

    companion object {
        fun init() = NewsFragment()
    }
}