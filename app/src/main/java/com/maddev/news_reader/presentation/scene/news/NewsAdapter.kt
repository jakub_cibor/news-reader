package com.maddev.news_reader.presentation.scene.news

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.maddev.news_reader.R
import com.maddev.news_reader.domain.entity.Article

class NewsAdapter(private val onClickListener: OnClickListener) : ListAdapter<Article, ArticleViewHolder>(ArticleDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder =
        ArticleViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.viewholder_article, parent, false))

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
        getItem(position).let { article ->
            holder.bind(article)
            holder.itemView.setOnClickListener { onClickListener.onClick(article) }
        }
    }

    interface OnClickListener {
        fun onClick(article: Article)
    }
}

private object ArticleDiffCallback : DiffUtil.ItemCallback<Article>() {
    override fun areItemsTheSame(oldItem: Article, newItem: Article): Boolean = oldItem == newItem
    override fun areContentsTheSame(oldItem: Article, newItem: Article): Boolean = oldItem == newItem
}