package com.maddev.news_reader.presentation.utils

import android.os.Bundle
import androidx.fragment.app.Fragment

inline fun <reified T : Fragment> T.addBundle(block: Bundle.() -> Unit): T = apply {
    arguments = Bundle().apply(block)
}