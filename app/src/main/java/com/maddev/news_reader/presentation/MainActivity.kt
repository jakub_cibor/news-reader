package com.maddev.news_reader.presentation

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.commitNow
import com.maddev.news_reader.R
import com.maddev.news_reader.presentation.scene.news.NewsFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) loadInitialFragment()
    }

    private fun loadInitialFragment() {
        supportFragmentManager.commitNow {
            add(R.id.container, NewsFragment.init())
        }
    }
}