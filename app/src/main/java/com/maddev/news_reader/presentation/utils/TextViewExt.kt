package com.maddev.news_reader.presentation.utils

import android.view.View
import android.widget.TextView

fun TextView.set(value: String?) {
    if (value.isNullOrEmpty()) visibility = View.GONE
    text = value
}