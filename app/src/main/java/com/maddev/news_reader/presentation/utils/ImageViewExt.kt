package com.maddev.news_reader.presentation.utils

import android.view.View
import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.viewholder_article.view.*

fun ImageView.load(src: String?) {
    if (src.isNullOrEmpty()) {
        image.visibility = View.GONE
    } else {
        val circularProgressDrawable = CircularProgressDrawable(context)
        circularProgressDrawable.centerRadius = 30f
        circularProgressDrawable.strokeWidth = 5f
        circularProgressDrawable.start()

        Glide.with(context)
            .load(src)
            .placeholder(circularProgressDrawable)
            .centerCrop()
            .into(image)
    }
}