package com.maddev.news_reader.di

import com.maddev.news_reader.presentation.scene.news.NewsViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel { NewsViewModel(get(), get()) }
}