package com.maddev.news_reader.di

import com.maddev.news_reader.domain.interactor.NewsInteractor
import com.maddev.news_reader.domain.interactor.NewsInteractorImpl
import org.koin.dsl.module

val interactorModule = module {
    factory<NewsInteractor> { NewsInteractorImpl(get(), get()) }
}