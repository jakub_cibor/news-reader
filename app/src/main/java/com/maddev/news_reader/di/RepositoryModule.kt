package com.maddev.news_reader.di

import com.maddev.news_reader.data.repository.NewsRepository
import com.maddev.news_reader.data.repository.NewsRepositoryImpl
import org.koin.dsl.module

val repositoryModule = module {
    single<NewsRepository> { NewsRepositoryImpl(get()) }
}