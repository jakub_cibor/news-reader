package com.maddev.news_reader.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.maddev.news_reader.data.Const.Api.URL
import com.maddev.news_reader.data.api.Api
import com.maddev.news_reader.data.api.HttpErrorMapper
import com.maddev.news_reader.data.api.RxCallAdapterFactory
import okhttp3.OkHttpClient
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

val apiModule = module {

    factory<Gson> {
        GsonBuilder().create()
    }

    factory {
        HttpErrorMapper()
    }

    single<OkHttpClient> {
        OkHttpClient()
            .newBuilder()
            .build()
    }

    single<Api> {
        Retrofit.Builder()
            .client(get())
            .baseUrl(URL)
            .addConverterFactory(GsonConverterFactory.create(get()))
            .addCallAdapterFactory(RxCallAdapterFactory.create(get()))
            .build()
            .create(Api::class.java)
    }
}