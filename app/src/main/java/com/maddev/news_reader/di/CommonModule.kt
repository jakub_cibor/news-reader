package com.maddev.news_reader.di

import com.maddev.news_reader.utils.SchedulersProvider
import com.maddev.news_reader.utils.SchedulersProviderImpl
import org.koin.dsl.module

val commonModule = module {
    single<SchedulersProvider> { SchedulersProviderImpl() }
}