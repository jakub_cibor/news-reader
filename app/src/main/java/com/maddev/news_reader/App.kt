package com.maddev.news_reader

import android.app.Application
import com.maddev.news_reader.di.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        registerDependencies()
    }

    private fun registerDependencies() {
        val list = listOf(
            repositoryModule,
            interactorModule,
            viewModelModule,
            commonModule,
            apiModule
        )

        startKoin {
            androidContext(this@App)
            androidLogger()
            modules(list)
        }
    }
}